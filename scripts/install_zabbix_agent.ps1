$serverhost = $args[0]
$zabbix_hostname_item = $args[1]

$service = Get-Service -Name 'Zabbix Agent' -ErrorAction SilentlyContinue
if ($service.Length -eq 0) {
  wget http://www.zabbix.com/downloads/3.0.4/zabbix_agents_3.0.4.win.zip -OutFile C:\zabbix_agent.zip
  Function Unzip-File()
  {
      param([string]$ZipFile,[string]$TargetFolder)
      if(!(Test-Path $TargetFolder))
      {
          mkdir $TargetFolder
      }
      $shellApp = New-Object -ComObject Shell.Application
      $files = $shellApp.NameSpace($ZipFile).Items()
      $shellApp.NameSpace($TargetFolder).CopyHere($files)
  }
  Unzip-File -ZipFile C:\zabbix_agent.zip -TargetFolder C:\zabbix_agent
  rm C:\zabbix_agent\conf\zabbix_agentd.win.conf

  Add-Content -value "LogFile=C:\zabbix_agent\zabbix.log" C:\zabbix_agent\conf\zabbix_agentd.win.conf
  Add-Content -value  "Server=$serverhost" C:\zabbix_agent\conf\zabbix_agentd.win.conf
  Add-Content -value "Hostname=$zabbix_hostname_item" C:\zabbix_agent\conf\zabbix_agentd.win.conf
  Add-Content -value  "ServerActive=$serverhost" C:\zabbix_agent\conf\zabbix_agentd.win.conf

  C:\zabbix_agent\bin\win32\zabbix_agentd.exe -c C:\zabbix_agent\conf\zabbix_agentd.win.conf -i
  Start-Service "Zabbix Agent"
  rm C:\zabbix_agent.zip
}

